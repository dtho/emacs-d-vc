;; items in 00.el, 01.el, . . .  are ordered
;; primarily by priority/desirability/utility

;;
;; Things that should be in 01a or 01b --
;; -- but these seem to slow Windows Emacs startup way down
;;

;; org tweaks
(load "dat-org.el")                     ; unhijack is esp. needed

;; time       -- date/time calculations, parsing, etc.
(unless (require 'ts nil t)
  (warn "ts is missing"))

;;
;; Everything else
;;

;; Bible
(cond ((eq window-system 'w32)
       (message "~/thomp/src/dtk/dtk.el is not loaded in Windows environment."))
      (t
       (load "/home/thomp/src/dtk/dtk.el")
       (load "/home/thomp/src/dtk-modules/lexicons-dictionaries/daily-light.el")))

;; file paths
(cond ((eq window-system 'w32)
       ;; Set to directory where most activity is taking place
       (setf abbreviated-home-dir "\\`c:/Users/Alan\\.Thompson/OneDrive - Fresno Pacific University\\(/\\|\\'\\)")))

;; pastebin
;(require '0x0)
(require 'webpaste)
; pop up a transient/temporary buffer
(add-hook 'webpaste-return-url-hook
          (lambda (url)
            (qtb-quick-temp-buffer)
            (insert url)))

      
(cond ((eq window-system 'w32)
       (message "stopping at 02.el -- 03.el is not loaded in Windows environment."))
      (t
       (load "03.el")))
