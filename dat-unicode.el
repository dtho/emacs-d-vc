
;; on laptop F6 maps to XF86AudioNext
;;(define-key org-mode-map [f6] 'dcs-popup)
;;(define-key org-mode-map (kbd "<XF86AudioNext>") 'dcs-popup)

;; Formerly (above), dcs was activated mode-by-mode. Are there any modes where dcs isn't desirable bound to f6?

;; test popup install since seems to broken on Windows Emacs
(cond ((require 'popup nil t)

       ;; ;;(load "char-selector.el") ; https://codeberg.org/thomp/d-char-selector
       ;;(require 'char-selector)
       
       (cond ((ignore-errors
	         (progn
                   (require 'char-selector "char-selector.el" t)
		   t)
	         )
              (global-set-key [f6] 'dcs-popup)
              (global-set-key (kbd "<XF86AudioNext>") 'dcs-popup)
              )
             (t
              (warn "Unable to load char-selector.el")))
       )
      (t
       (warn "char-selector.el relies on popup")))


