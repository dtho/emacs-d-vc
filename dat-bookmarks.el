;;
;; bookmarks
(setq bookmark-save-flag t)
(setq bookmark-default-file                 ; use a portable location for bookmark-default-file
      (cond ((eq window-system 'x)
	     "/home/thomp/.emacs.d/bookmarks-dat-linux.bmk")
	    ((eq window-system 'w32)
	     ;; ensure this is set correctly for Windows
	     (setq bookmark-default-file
                   ;; c:/Users/Alan.Thompson/OneDrive - Fresno Pacific University/emacs-d/bookmarks
		   "c:/Users/Alan.Thompson/OneDrive - Fresno Pacific University/emacs-d/bookmarks"))
	    ))

(unless (file-exists-p bookmark-default-file)
  (warn (format "Unable to locate bookmark-default-file -- %s" bookmark-default-file)))

;;
;; Custom bookmarks key bindings
;;

;; bookmark-jump
(define-key global-map [f9] 'bookmark-jump)
;; ;; f9 may be highjacked by a search box popup (KDE? xfce?)
(global-set-key (kbd "<XF86Search>") 'bookmark-jump)
(global-set-key (kbd "<f9>")         'bookmark-jump)
;; Fallback if f9 keybinding is a hassle
(global-set-key (kbd "C-c 9")        'bookmark-jump)

;; jump to todos
(keymap-global-set "C-c 8"
                   ;;'bookmark-jump
                   (lambda ()
                     (interactive)
                     (find-file (cond ((eq window-system 'w32)
                                       "c:/Users/Alan.Thompson/OneDrive - Fresno Pacific University/00/00TODO.org")
                                      (t
                                       "/home/thomp/00todo1.org")))
                     )
                   )
