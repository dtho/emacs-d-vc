;; ? - why doesn't dream-theme show up under custom themes list?

;; Anticipate possibility dream-theme isn't loaded
;;https://github.com/djcb/dream-theme
(unless (require 'dream-theme nil t)
  (warn "dream-theme not loaded"))
;; Anticipate possibility dream-theme isn't installed
(when (featurep 'dream-theme)
  (enable-theme 'dream))
;; dream default:
;; Foreground: #4b5262
;;             ^^ too dim
(let ((brighter-fg "#5b6272"))
  (set-face-attribute 'font-lock-doc-face nil
                      :foreground brighter-fg
                      :background 'unspecified)
;; FIXME: anticipate:
;; error: Invalid face, font-lock-delimiter-face
;;  (set-face-attribute 'font-lock-delimiter-face nil :foreground brighter-fg :background nil)
  (set-face-attribute 'font-lock-comment-face nil
                      :foreground brighter-fg
                      :background 'unspecified)
  )
