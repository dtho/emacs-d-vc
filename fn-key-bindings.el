;; All customized function key bindings should be here


                                        ;<f1> ? runs the command help-for-help (found in global-map), which is
                                        ;<f2> ?
                                        ;<f3> ?
                                        ;<f4> ?

;;
;; listed in terms of frequency of use
;;



;; frequently used slime stuff
(global-set-key [f10]         'slime-repl*)
(global-set-key (kbd "C-c 0") 'slime-repl*)
;; FIXME: these would probably be better limited to lisp mode
;; OLD:
;;(global-set-key [f11] 'slime-fuzzy-complete-symbol)
;;(global-set-key [f12] 'slime-complete-form)
;; NEW:
(define-key lisp-mode-map [f11] 'slime-fuzzy-complete-symbol)

;; f12 -> <XF86MonBrightnessUp> is undefined
(define-key lisp-mode-map [f12] 'slime-complete-form)


(when (boundp 'org-mode-map)
  (define-key org-mode-map [f5] 'org-latex-export-to-pdf)
  ;; on laptop F5 maps to XF86AudioPlay
  (define-key org-mode-map (kbd "<XF86AudioPlay>") 'org-latex-export-to-pdf))

(global-set-key [f6] 'dcs-popup)
(global-set-key (kbd "<XF86AudioNext>") 'dcs-popup)

(message "f-k-b 10")


(global-set-key [f8] 'zoom-mode)	;toggle zoom mode

(message "f-k-b 20")





(message "f-k-b 40")

