;; dat-new-issue-org
(defun dat-new-issue-org ()
  (interactive)
  ;; assume point is at/near location of desired insertion
  ;;(goto-char (point-min))
  (beginning-of-line)
  ;; grab issue summary
  (let ((issue-summary (read-from-minibuffer "Issue summary: "))
        (date (ts-format (ts-now))))
    (insert "* OPEN " issue-summary
            "
filed: " date "

** email: " date "

 from: DAT
 subj: issue: " issue-summary "
 to:
 body: " issue-summary "
"
 )))

