;; sequence of 00.el, 01.el, . . .  is ordered
;; primarily by relative priority-desirability-utility

;; 00.el - near-essentials

;; appearance
(load "dat-appearance.el")

;; org - loads slow when trying to load first org file on Windows
;; kludge: load org when emacs is idle for at least 2 s
(run-with-idle-timer
 2 nil
 (lambda ()
   (require 'org)
   ;; wrap lines - don't truncate

   ;; org-startup-truncated
   ;; Non-nil means entering Org mode will set ‘truncate-lines’ -- useful since some lines containing links can be very long and uninteresting.  Also tables look terrible when wrapped.
   (setf org-startup-truncated nil)
   ;; (add-hook 'org-mode-hook 'turn-on-visual-line-mode)
   ))

;; ;; bookmarks
(load "dat-bookmarks.el")

;; misc. emacs tweaks
(load "dat-misc.el")

(load "01a.el")
