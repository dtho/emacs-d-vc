;;
;; mode line
;;

;; option 1: keep it simple w/ feebleline
;; - purchases additional screen space
;; - reduces screen noise
(unless (ignore-errors
	  (progn (require 'feebleline "feebleline.el" t)
                 (feebleline-mode)
                 (push '((lambda () (format-time-string "[%H:%M %Y-%m-%d]")) :post "") feebleline-msg-functions)
		 t)
	  )
  (warn "Unable to load feebleline.el"))


;; option 2: customize default mode line

;; ;; mode-line-format is a variable
;; ;; - specifies a template for displaying mode line for a window’s buffer
;; ;; - the value may be nil, a string, a symbol or a list.
;; (defvar mode-line-format-list
;;   nil)

;; (setq mode-line-format-list
;;       '(;"aaaa"
;;         "%e"
;;         mode-line-front-space
;;         ;; Standard info about the current buffer
;;         ;mode-line-mule-info
;;         ;mode-line-client
;;         mode-line-modified
;;         mode-line-remote
;;         mode-line-frame-identification
;;         mode-line-buffer-identification " " mode-line-position
;;         ;; Some specific information about the current buffer:
;;         ;lunaryorn-projectile-mode-line          ; Project information
;;         (vc-mode lunaryorn-vc-mode-line) ; VC information
;;         ;(flycheck-mode flycheck-mode-line) ; Flycheck status
;;         ;(multiple-cursors-mode mc/mode-line) ; Number of cursors
;;         " "
;;         ;; Misc information, notably
;;         ;; battery state and function name
;;         ;; persp-modestring
;;         mode-line-misc-info
;;         " "
;;         ;; don't show minor mode
;;         ;;mode-line-modes
;;         ;; Major mode
;;         "%m"
;;         ;mode-line-end-spaces
;;         )
;;       )

;; (setq-default mode-line-format
;;               mode-line-format-list)

;; ;; display date alongside time
;; ;; - see also display-time-string
;; (setq display-time-day-and-date t)
;; (display-time)

;; ;; utility fn for experimenting with mlfl
;; (defun dat-apply-mlfl ()
;;   (interactive)
;;   (setq mode-line-format
;;         mode-line-format-list))

