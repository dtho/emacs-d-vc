;; items in 00.el, 01.el, . . .  are ordered
;; primarily by priority/desirability/utility

;; packages
(load "dat-packages.el")

;; passwords
(cond ((eq window-system 'w32)
       (message "Not setting up passwords"))
      (t
       (load "dat-passwords.el")))

;; Common Lisp (SLIME) and elisp
(cond ((eq window-system 'w32)
       (message "Not setting up Common Lisp"))
      (t
       (load "dat-lisp.el")))

;; diary
(cond ((eq window-system 'w32)
       (message "Diary is not loaded in Windows environment."))
      (t
       (load "dat-diary.el")))

;; unicode character selector
(load "dat-unicode.el")

;; frequent actions - map these to function keys
(load "fn-key-bindings.el")

;; define indentation
;; - no tabs in source code, etc.
(load "dat-whitespace.el")

;; mode line
(load "dat-mode-line.el")

;; Handle changes in window configuration
(winner-mode 1)

(cond ((eq window-system 'w32)
       (message "Not loading additional (02.el) config"))
      (t
       (load "02.el")))
