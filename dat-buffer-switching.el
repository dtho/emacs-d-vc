;;
;; buffer switching and emacs window navigation
;;


;;
;; navigating between emacs windows (global)

;; too many apps use Alt-[x] combinations, i3 uses Super, and shift-arrow combinations are used by org-mode  -> what's left for windmove?? 
;; A: Ctrl-Shift-arrow key combos (only downside is that org-mode hijacks these a well...)
(progn
  (global-set-key [C-S-left] 'windmove-left) 
  (global-set-key [C-S-right] 'windmove-right) 
  (global-set-key [C-S-up] 'windmove-up) 
  (global-set-key [C-S-down] 'windmove-down))

;;
;; buffer switching

;; ido is *great* for switching between current buffers
;; - your friends are 'C-x b' etc. 
;; - use M-s to have ido look in history, etc.
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)
