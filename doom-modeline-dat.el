;;
;; doom-modeline 
;;
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(doom-modeline-mode)

(setf display-time-day-and-date t)
(display-time)				;Enable display of time, load level, and mail flag in mode lines.
;; or (display-time-mode 1)

;; Don't show icons in the mode-line
(setq doom-modeline-icon nil)
;; Set display of the minor modes
(setq doom-modeline-minor-modes nil)
(setq doom-modeline-checker-simple-format nil) 
(setq doom-modeline-buffer-file-name-style 'buffer-name) 

;; doom-modeline font size
;; M-x > customize-face > mode-line and adjust your height from there.

;; this only works down to a minimum height imposed by modeline font height
(setq doom-modeline-height 1)		; set height to minimum

;; dired paths can be painfully long - truncate doesn't seem to be honored
(doom-modeline-def-segment buffer-info-dired
  "Information about the current dired buffer."
  ;; Like truncate-upto-project but kludge to show full name of directory
  (let ((shrunk (shrink-path-prompt (abbreviate-file-name default-directory))))
    (doom-modeline--buffer-file-name-truncate
     ;; kludge
     (concat
      (abbreviate-file-name default-directory)
      ;(cdr shrunk)			; subdir name
      )
     default-directory
     nil ; t
     )))

(doom-modeline-def-modeline 'main
  ;; redefine order of items -- put date/time 1st
  '(misc-info				
    bar workspace-name window-number modals matches buffer-info remote-host buffer-position word-count parrot selection-info)
  '(objed-state persp-name battery grip irc mu4e gnus github debug lsp minor-modes input-method indent-info buffer-encoding major-mode)
  )

(doom-modeline-def-modeline 'project
  ;; redefine order of items -- put date/time 1st
  '(misc-info bar window-number
	      ;; Dired buffer already has dir @ the top of the buffer
	      ;;buffer-info-dired		;buffer-default-directory ; shows default-directory
	      )
  '(battery github debug major-mode process))

;; customate date/time appearance in the modeline
(setf display-time-string-forms '(year "-" month "-" day
                                       " " 24-hours ":" minutes
                                       " " (substring dayname nil 3)
                                       ))
