(when (>= emacs-major-version 24)
  (require 'package)
  ;;  Default:
  ;; > package-archives
  ;; (("gnu" . "https://elpa.gnu.org/packages/")
  ;;  ("nongnu" . "https://elpa.nongnu.org/nongnu/"))
  (add-to-list 'package-archives
	       '("melpa" . "https://melpa.org/packages/") t))

;;
;; complain if packages we expect to be available are missing
;;
(let ((dat-expected-packages
       '(
	 ;dired-launch
	 ;paredit
	 ;zygospore			; reversible C-x 1 (delete-other-windows)
	 )))
  ;; additional packages when using personal laptop
  (when (not (eq window-system 'w32))
    (setq dat-expected-packages
	  (append '(
		    ;gscholar-bibtex ; Retrieve BibTeX from Google Scholar and other online sources(ACM, IEEE, DBLP)
		    ;lorem-ipsum	 ; Insert dummy pseudo Latin text.
		    ;magit				; A Git porcelain inside Emacs
		    ;perspective
		    ;web-mode	    ; major mode for editing web templates
		    ;yasnippet	    ; Yet another snippet extension for Emacs.
		    )
		  dat-expected-packages)))
  (mapc #'(lambda (package-symbol)
	    (if (not (package-installed-p package-symbol))
		(warn (format "Package %s expected but not present" package-symbol))))
	dat-expected-packages))
