;;
;; dired preferences, extras, and utility functions
;;
(require 'dired)

(cond ((require 'dired-launch nil t)
       (add-hook 'dired-mode-hook
	         (lambda () (dired-launch-mode)))
       ;;
       ;; define dired-launch application defaults
       (setf dired-launch-extensions-map
             '(("docx" ("lowriter" "swriter"))
	       ("odp"  ("loimpress" "simpress"))
	       ("ods"  ("localc" "scalc"))
	       ("odt"  ("libreoffice" "soffice"))
	       ("JPG"  ("phototonic" "gimp"))
	       ("pdf"  ("okular" "qpdfview" "evince" "xournalpp"))
	       ("png"  ("phototonic"))
	       ("svg"  ("inkscape"))
	       ("webm" ("haruna"))
	       ("html" ("firefox")))
             )
       ;; quick launch of file manager on dired dir
       (cond ((eq system-type 'windows-nt)
              ;; https://raw.githubusercontent.com/thomp/dired-launch/master/dired-launch-x-on-dir.el
              (load "dired-launch-x-on-dir.el"))
             (t
              (load "/home/thomp/src/dired-launch/dired-launch-x-on-dir.el")))
       )
      (t
       (warn "dired-launch not requirable")))

(require 'dired-x)	   ; provides dired-omit-mode (omit dot files)

(add-hook 'dired-mode-hook
	  (lambda ()
	    ;; keep things visually clean by default
            (dired-hide-details-mode)
	    ))

;;
;; hide 'dot' files by default

;; https://emacs.stackexchange.com/questions/68585/dired-mode-toggle-show-hidden-files-folders-by-keyboard-shortcut

;; default does not hide dot files (.git, etc.); default: "\\`[.]?#\\|\\`[.][.]?\\'"
(setq dired-omit-files
      (rx (or (seq bol (? ".") "#")     ;; emacs autosave files
              (seq bol "." (not (any "."))) ;; dot-files
              (seq "~" eol)                 ;; backup-files
              (seq bol "CVS" eol)           ;; CVS dirs
              )))

;; 'h' to show/hide hidden files
(define-key dired-mode-map
            (kbd "h")
            'dired-omit-mode)






