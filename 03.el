;; items in 00.el, 01.el, . . .  are ordered
;; primarily by priority/desirability/utility

;; 0blayout-new, 0blayout-kill, 0blayout-switch
;;(require '0blayout)


;; statistics basics - no ESS, no Emacs calc wierdness
(defun mean (&rest x)
  (/ (apply '+ x) (length x))
  )


;; M-x stopwatch
(load "/home/thomp/.emacs.d/stopwatch/stopwatch.el")
;; however, see also:
;; https://github.com/lalopmak/stopwatch/ <-- forked from 
;; https://github.com/blue0513/stopwatch  ; M-x stopwatch-start
;; https://github.com/asnr/stopwatch
;; https://github.com/suchunyang/countdown.el
;; https://github.com/positron-solutions/champagne


					;(load "04.el")

