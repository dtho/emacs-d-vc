;; 01a.el

;; dired
(load "dat-dired1.el")
(setq dired-listing-switches "-alG")    ; don't show group

;; system bell - problematic on Windows unless dealt with
(setq ring-bell-function 'ignore)

;; time       -- displaying time
(display-time)


;;;;;;;;;;;;;; absence of content below is a bit less noticeable

;; appearance
(load "dat-theme.el")

(setq package-install-upgrade-built-in t)

;; buffer switching and emacs window navigation
(load "dat-buffer-switching.el")

;; backup files
(load "dat-backup-files.el")

;; quick temporary buffers
; DAT laptop: /home/thomp/.emacs.d/mktmp-el
; ~/src/mktmp-el
;https://gist.github.com/thomp/0a8ed29b2094fdf43536a151722ebe6a
; wget https://gist.github.com/thomp/0a8ed29b2094fdf43536a151722ebe6a -OutFile qtb.el
(unless (ignore-errors
	  (require 'qtb "qtb.el" t))
  (warn "Unable to load qtb.el. The qtb.el gist location: https://gist.github.com/thomp/0a8ed29b2094fdf43536a151722ebe6a"))
;; custom key binding for qtb
(global-set-key (kbd "<f7>") 'qtb-quick-temp-buffer)

;; email - composing replies
(load "dat-mail-replies.el")

(load "01b.el")
