;; whitespace

;; tabs
(setq-default indent-tabs-mode nil)

;; whitespace mode customizations
(require 'whitespace)
;; Customize whitespace faces
(set-face-attribute 'whitespace-space nil
                    :background "#21242B"     ; default:      Background: #181b1f
                    :foreground "gray30")

;; whitespace-newline

;; font-lock-comment-face
(set-face-attribute 'whitespace-line nil
                    :background nil ;"black" ; "gray11"
                    :foreground "chocolate2")
