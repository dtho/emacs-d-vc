(message "Loading dat-appearance.el")
;;
;; appearance
;;

;;
;; colors
(defvar dat-bgd-color "#121212") ; "#121212" provides *much* better contrast than  "#151515" ; "#202020" "grey21"
(defvar dat-fgd-color "gainsboro")
(set-mouse-color "red")
(set-background-color dat-bgd-color)
(set-foreground-color dat-fgd-color)
(set-border-color "white") ; border of the frame

;;
;; cursor
(set-cursor-color "white")
(setq blink-cursor-alist '((box . bar) (hollow . bar)))

;;
;; fonts

;; only use X windows-related fonts when available (window-system is NIL when emacs is in console mode)

;; from https://www.emacswiki.org/emacs/GoodFonts
;; see also https://gist.githubusercontent.com/haxney/3055728/raw/8f840c22bf350987be7e90fef1dd009f688f28c2/gistfile1.el
(defun dat-preview-available-fonts ()
  (text-scale-increase 2)
  (let ((str "The quick brown fox jumps over the lazy dog ´`''\"\"1lI|¦!Ø0Oo{[()]}.,:; ")
        (font-families (cl-remove-duplicates
                        (sort (font-family-list)
                              (lambda(x y) (string< (upcase x) (upcase y))))
                        :test 'string=)))
    (dolist (ff font-families)
      (insert
       (propertize str 'font-lock-face `(:family ,ff))               ff "\n"
       (propertize str 'font-lock-face `(:family ,ff :slant italic)) ff "\n")))
  nil)

;; set default font on a Windows machine
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Fonts.html

;; 1. Use M-x customize-face
;; 2. Get value from < .emacs ? >
(defun dat-set-default-win-face ()
  (interactive)
  (let ((default-face-spec
         ;; WINDOWS BUILTINS
         
	 ;; Consolas
	 ;;"-outline-Consolas-normal-normal-normal-mono-20-*-*--c--iso8859-1"
	 ;;"-outline-Consolas-normal-normal-normal-mono-24-*-*--c--iso8859-1"
	 ;;"-outline-Consolas-normal-normal-normal-mono-35-*-*--c--iso8859-1"
	 ;;"Consolas-12"
         (list :inherit nil :extend nil :stipple nil
               :background dat-bgd-color :foreground dat-fgd-color
               :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant 'normal
               :weight 'regular
               :height 180 :width 'normal
               :foundry "outline"
               :family "Consolas")
         
         ;;
         ;; NOT AVAILABLE IN DEFAULT WINDOWS INSTALL
         ;;

         ;; DejaVu Sans Mono
         
	 ;; Liberation Mono
	 ;;(set-face-attribute 'default nil :font "Liberation Mono-14" :slant 'normal :weight 'normal)
	 
	 ;; Linux Biolinum G

	 ;; Noto Mono
         
	 ;; Source Code Pro
	 ;; - 27 works
	 ;; - 29 works
	 ;; - 31 works
	 ;; - 35 no good
	 ;;"-outline-Source Code Pro-normal-normal-normal-mono-27-*-*-*-c-*-iso8859-1"
	 ;;"-outline-Source Code Pro-normal-normal-normal-mono-29-*-*-*-c-*-iso8859-1"
	 ;;"-outline-Source Code Pro-normal-normal-normal-mono-31-*-*-*-c-*-iso8859-1"
	 ;;"-outline-Source Code Pro-normal-normal-normal-mono-35-*-*-*-c-*-iso8859-1"
	 ;;"-outline-Source Code Pro-normal-normal-normal-mono-37-*-*-*-c-*-iso8859-1"

	 ;; Miriam Mono CLM	 
         ))
    ;; Q: Do we need to set DEFAULT-FRAME-ALIST? Or is calling SET-FACE-ATTRIBUTE sufficient?
    ;(add-to-list 'default-frame-alist (cons `font default-font))
    (apply 'set-face-attribute
           (append (list 'default t)
                   default-face-spec))
    ))


(cond ((eq window-system 'x)
       (set-face-attribute 'default nil :font "Fira Code-21")
       )
      ((eq window-system 'w32)
       (dat-set-default-win-face)
       ;; Are Miram fonts available?
       (when (find-font (font-spec :name "Miram Mono CLM-22"))
         ;; Anticipate possibility org isn't loaded
         (when (facep 'org-meta-line)
	   (set-face-attribute 'org-meta-line nil :font "Miriam Mono CLM-22" :slant 'normal :weight 'normal))
         (when (facep 'org-table)
	   (set-face-attribute 'org-table nil :font "Miriam Mono CLM-22" :slant 'normal :weight 'normal)))
       )
      )
