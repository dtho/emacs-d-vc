;; handle FPU-specific stuff

; generate emails from sunbird central roster
(defun dat-fpu-sc-roster-csv-emails ()
  "Invoke from the buffer containing CSV data."
  (interactive)
  ;; copy to new buffer
  (let ((raw-string (buffer-string))
	(buffer-name "* DAT FPU roster emails *"))
    (let ((new-buffer (get-buffer-create buffer-name)))
      (switch-to-buffer new-buffer)
      (insert raw-string)
      ;; now grab emails
      (beginning-of-buffer)
      (kill-line)			; header line
      (kill-line)
      (csv-mode)				; ensure sexp fns work
      ;; unless at end of file
      (let ((lines-left (count-lines (point)
				     (point-max)
				     )))
	(while (> lines-left 0)
	  (dat-fpu-grab-email)
	  (setq lines-left (count-lines (point)
					(point-max)
					))
	  (next-line)))
      )
    ))

;; helper for dat-fpu-sc-roster-csv-emails - handle a single csv row - assume at start of roster line
(defun dat-fpu-grab-email ()
  (interactive)
  (move-end-of-line 1)
  (delete-backward-char 1)
  (backward-sexp)
  (forward-char)
  (let ((start (point)))
    (beginning-of-line)
    (delete-region start (point)))
  )
