;;
;; misc settings
;;

;; suppress splash screen
(setq inhibit-splash-screen t)

;; no need to hassle user about the [dated] "two-spaces-after-a-period" typographical convention
(setq sentence-end-double-space nil)

;; inadvertently pressing C-z w/default binding is annoying
(put 'suspend-frame 'disabled t)

;; keep more messages around...
(setq message-log-max 300)

;; to use M-% with i3 window manager...
(global-unset-key "\M-%")

;; no need for scroll bar, tool bar, or menu bar except, perhaps, when learning keybindings
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

;; commands we'd like to have available...
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; ask for confirmation when leaving Emacs
(setq confirm-kill-emacs 'yes-or-no-p)
