(require 'org)

;; wrap lines - don't truncate
(setf org-startup-truncated nil)
;; (add-hook 'org-mode-hook 'turn-on-visual-line-mode)

;; set initial view to folded
(setf org-startup-folded 'show2levels)

;; lists
(setq org-list-allow-alphabetical t)

;;
;; org LaTeX export
;;

;; default headers for org LaTeX export
(defun dat-insert-org-latex-header ()
  (interactive)
  (goto-char (point-min))
  (insert "#+TITLE: --
#+OPTIONS: toc:nil date:nil author:nil
#+OPTIONS: num:nil
#+LATEX_COMPILER: xelatex
#+LaTeX_HEADER: \\usepackage{extsizes}
#+LaTeX_HEADER: \\usepackage[top=0.5in,bottom=0.5in,portrait,letterpaper,includeheadfoot]{geometry}
#+LaTeX_HEADER: \\usepackage{parskip} \\usepackage{nopageno} \\usepackage{fancyhdr}
#+LATEX_CLASS: extarticle
#+LaTeX_class_options: [14pt]
# +LaTeX_class_options: [12pt]
#+LaTeX_HEADER: \\pagestyle{fancy} \\fancyhf{} \\fancyhead[C]{ } \\fancyhead[R]{\\thepage} \\fancyhead[L]{BIOL 421} \\fancyfoot[R]{\\today}
#+LATEX_HEADER: \\renewcommand\\maketitle{}

# If on Windows keep this commented out and just stick with default fonts.
# If on Linux, it should be safe to use DejaVu fonts or others.
# +LaTeX_HEADER: \\usepackage{xltxtra} \\usepackage{fontspec} \\setmainfont{DejaVu Serif} \\setsansfont{DejaVu Sans} \\setmonofont{DejaVu Sans Mono}

# if using XeLaTeX
# (setq org-latex-compiler \"xelatex\")

# insert current date and time
{{{time(%Y-%m-%d %a %H:%M)}}}

")
  )

;; support extsizes
;; (require 'ox-latex) necessary for org-latex-classes to be defined
(defun dat-org-latex-support-extsizes ()
  (interactive)
  (push '("extarticle" "\\documentclass[11pt]{article}"
	  ("\\section{%s}" . "\\section*{%s}")
	  ("\\subsection{%s}" . "\\subsection*{%s}")
	  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	  ("\\paragraph{%s}" . "\\paragraph*{%s}")
	  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	org-latex-classes))

(require 'ox-latex)
(dat-org-latex-support-extsizes)

;;
;; key bindings
;;

;;
;; bind fN keys to commonly-used commands
;;
;;    f5      org-latex-export-to-pdf
;;    f6      unicode char selector
;;

;; see http://orgmode.org/manual/Conflicts.html#Conflicts

;; Although org-mode leaves M-f and M-b available for forward-word and
;; backward-word, it hijacks the M-right and M-left default key
;; bindings, binding to org-metaright and org-metaleft, respectively.
(defun org-unhijack-word-bindings ()
  (define-key org-mode-map (kbd "M-<right>")
    'forward-word			; or NIL
    )
  (define-key org-mode-map (kbd "M-<left>") 'backward-word))

;; Don't let org run roughshod over the windmove bindings we prefer
(defun org-unhijack-windmove-bindings ()
  (define-key org-mode-map [C-S-left]
    'windmove-left			; or NIL
    )
  (define-key org-mode-map [C-S-right] 'windmove-right)
  (define-key org-mode-map [C-S-up] 'windmove-up)
  (define-key org-mode-map [C-S-down] 'windmove-down)
  )

(defun org-unhijack-bindings ()
  (interactive)
  (org-unhijack-word-bindings)
  (org-unhijack-windmove-bindings))

;; This triggers 'user-error: Minibuffer is inactive'.
;; The error is triggered irrespective of whether org-unhijack-bindings is interactive or not.

;;(eval-after-load "org" (org-unhijack-bindings))

(defun dat-org-key-bindings ()
  (interactive)
  (org-unhijack-word-bindings)
  (org-unhijack-windmove-bindings)
  
  (define-key org-mode-map [f5] 'org-latex-export-to-pdf)
  ;; on laptop F5 maps to XF86AudioPlay
  (define-key org-mode-map (kbd "<XF86AudioPlay>") 'org-latex-export-to-pdf)
  )


;; Approach #1:
;; Don't use this -- org was just loaded (see require @ top of file)
;;(eval-after-load "org-mode" '(progn (dat-org-key-bindings)))

;; Approach #2: hooks
;; - are these problematic? why?
;; (add-hook 'org-mode-hook
;; 	  (lambda ()
;; 	    ;; on some systems, function keys map to XF86Audio
;; 	    (define-key org-mode-map [f6] 'ucs-char-menu)
;; 	    (define-key org-mode-map  (kbd "<XF86AudioNext>") 'ucs-char-menu)))

(dat-org-key-bindings)
