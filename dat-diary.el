;;;
;;; diary
;;;

;; support use of multiple diary files
(add-hook 'diary-list-entries-hook 'diary-include-other-diary-files) 
(add-hook 'diary-mark-entries-hook 'diary-mark-included-diary-files)

;; sort diary entries by time of day
(add-hook 'diary-list-entries-hook 'diary-sort-entries t)

;; diary file location (emacs default is ~/diary)
(setq diary-file "/home/thomp/personal/calendars--diaries/diaries/diary")

;; define how many days to show; order is Sun Mon Tue ... Sat
(setq diary-number-of-entries [15 4 2 2 2 15 15])

;; bring diary buffer to foreground @ start of each day
(defvar dat-diary-timer (run-at-time t 86400 'diary))

;; bring up the diary buffer					
(diary)
(switch-to-buffer diary-fancy-buffer)
