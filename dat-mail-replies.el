;;
;; email: composing replies
;;
(defun dat-indent-citation (&optional start end yank-only)
  (interactive)
  ;;(message-indent-citation)
  (let ((region-bound-1 (or start (point)))
	(region-bound-2 (or end (mark t)))
	)
    (if (> region-bound-1 region-bound-2)
	(setq start region-bound-2
	      end region-bound-1)
      (setq start region-bound-1
	    end region-bound-2))


    ;;(unless start (setq start (point)))
					;(unless end (setq end (mark t)))
    ;; Better would be to reduce fill-region width by length of message-yank-cited-prefix
    ;; The maximum line width for filling is specified by the buffer-local variable fill-column. The default value (see Local Variables) is 70. The easiest way to set fill-column in the current buffer is to use the command C-x f (set-fill-column). With a numeric argument, it uses that as the new fill column. With just C-u as argument, it sets fill-column to the current horizontal position of point.

    ;; move point so we can figure out where fill-region finished the job
    (goto-char end)
    (let ((preserve-fill-column fill-column))
      (set-fill-column (- fill-column (length message-yank-cited-prefix)))
      (fill-region start end nil nil nil)
      (set-fill-column preserve-fill-column)
      )
    
    (message "%s %s" end (point))
    ;; What is new region after fill-region?
    ;; seems like it should be point -- but that isn't the case -- fill-region returns point to original position
    
    (save-excursion
      (goto-char start)
      (while (< (point) (or end (mark t)))
	(cond ((looking-at ">")
	       (insert message-yank-cited-prefix))
	      ((looking-at "^$")
	       (insert message-yank-empty-prefix))
	      (t
	       (insert message-yank-prefix)))
	(forward-line 1)))
    )


  )
