;; don't litter directories with emacs backup files -> keep them all in one place
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)
